.. include:: images.rst

.. _erm-system-preferences-label:

E-Resource management
---------------------

The E-Resource management tab in Koha will give you settings to enable
and configure the :ref:`E-Resource management (ERM) module <erm-label>`.

-  Get there: More > Administration > System Preferences > E-Resource management

.. _erm-interface-label:

Interface
~~~~~~~~~

.. _erm-ermmodule-label:

ERMModule
^^^^^^^^^

Asks: \_\_\_ the e-resource management module

Values:

-  Disable

-  Enable

Default: Disable

Description:

-  This preference controls whether or not staff can use the :ref:`E-Resource management (ERM) module <erm-label>`.

-  This is the main switch for the E-Resource management functionality.

.. Note::

   To use the module, staff also need to have the
   :ref:`erm permission <permission-erm-label>` (or the
   :ref:`superlibrarian permission <permission-superlibrarian-label>`).

.. _erm-ermproviderebscoapikey-label:

ERMProviderEbscoApiKey
^^^^^^^^^^^^^^^^^^^^^^

Asks: API key for EBSCO HoldingsIQ \_\_\_

Description:

-  This preference allows libraries to integrate with the EBSCO global
    knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__
    tool. The library will need to ask EBSCO for both
    an API key and a Customer ID (see :ref:`ERMProviderEbscoCustomerID <erm-ermproviderebscocustomerid-label>` below).
    Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in
    E-Resource management.

-  If this preference is left empty, you will be unable to connect the EBSCO knowledge base.

.. _erm-ermproviderebscocustomerid-label:

ERMProviderEbscoCustomerID
^^^^^^^^^^^^^^^^^^^^^^^^^^

Asks: Customer ID for EBSCO HoldingsIQ \_\_\_

Description:

-  This preference allows libraries to integrate with the EBSCO global
    knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__
    tool. The library will need to ask EBSCO for both
    an API key and a Customer ID (see :ref:`ERMProviderEbscoApiKey <erm-ermproviderebscoapikey-label>` above).
    Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in
    E-Resource management.

-  If this preference is left empty, you will be unable to connect the EBSCO knowledge base.

.. _erm-ermproviders-label:

ERMProviders
^^^^^^^^^^^^

Asks: Providers for the e-resource management module \_\_\_

Values:

-  Select all

-  EBSCO

-  Local

Default: Local

Description:

-  This preference controls whether to show one or more sources of
    eHoldings in the :ref:`eHoldings <erm-eholdings-label>` menu option.

-  Selecting "EBSCO" allows integration with the EBSCO global knowledge base.

-  Selecting "Local" provides the ability to manually create eHoldings records for local sources.
