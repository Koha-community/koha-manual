.. include:: images.rst

.. _using-third-party-software-with-koha-label:

Using third party software with Koha
====================================

.. _marc-edit-label:

MarcEdit
-------------------------

Many libraries like to use MarcEdit for modifications or data cleanup
such as converting records in spreadsheet data formats to MARC records.
If you'd like to do this you will need to download it at:
http://marcedit.reeset.net/ 

    .. Note::

       Many of the actions described on the Koha Community wiki 
       at https://wiki.koha-community.org/wiki/MARCEdit can be done in Koha
       using :ref:`Marc Modification Templates <marc-modification-templates-label>`, but 
       instructions are provided on the Wiki for those who are used to MarcEdit.

.. _oclc-cataloging-services-label:

OCLC Cataloging Services
------------------------

Koha can be set to work with the OCLC cataloging services such as

- `WorldShare Record Manager <https://www.oclc.org/en/worldshare-record-manager.html>`_
- `Connexion <https://www.oclc.org/en/connexion.html>`_

This allows librarians to use the WorldShare Record Manager web interface or the
OCLC Connexion Client desktop software as their cataloging tool and send those
records to Koha with a single click.

.. _setting-up-oclc-service-on-koha-label:

Setting up the OCLC Connexion Daemon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, you will need to set up the
:ref:`OCLC Connexion Daemon <cron-connexion-import-daemon-label>` on your
server. If you have a system administrator you will want to consult with them on
this process.

1. Find the script on your server and view its documentation.

   .. code-block:: shell
   
        /usr/share/koha/bin/connexion_import_daemon.pl --help

2. Create a configuration file. You could put this anywhere that is readable by
   the user that will be running the service, e.g.,
   ``/etc/koha/sites/my_instance/oclc_connexion.conf``. The output of the help
   command provides the details about what this file should contain. Here is an
   example:
   
   ::
   
       host: 0.0.0.0
       port: 5500
       log: /var/log/koha/my_instance/oclc_connexion.log
       koha: https://staff.mylibrary.example.com
       user: koha_staff_user_name
       password: koha_staff_user_password
       connexion_user: oclc_connexion_user_name
       connexion_password: oclc_connexion_user_password
       import_mode: direct

3. Since the configuration file contains passwords, make sure that it's only
   readable by the user running the script, and nobody else.
   
   .. code-block:: shell
   
         chmod 400 /etc/koha/sites/my_instance/oclc_connexion.conf

4. Run the script.

   - You can do this manually to test it out:
     
     .. code-block:: shell
     
         /usr/share/koha/bin/connexion_import_daemon.pl --config /etc/koha/sites/my_instance/oclc_connexion.conf

   - Or you can set up a ``systemd`` unit to keep the script running even when
     it crashes or the server reboots:

     1. Create a file at ``/etc/systemd/system/koha-oclc-connexion.service``:
     
        .. code-block::
        
            [Unit]
            Description=Koha OCLC Connexion Daemon
            After=network.target
        
            [Service]
            Type=exec
            ExecStart=/usr/share/koha/bin/connexion_import_daemon.pl --config /etc/koha/sites/my_instance/oclc_connexion.conf
            Restart=always
        
            [Install]
            WantedBy=multi-user.target
  
     2. Enable and start the service:

        .. code-block:: shell
        
            systemctl enable koha-oclc-connexion
            systemctl start koha-oclc-connexion

     3. Check the status of the service:

        .. code-block:: shell
        
            systemctl status koha-oclc-connexion

WorldShare Record Manager
~~~~~~~~~~~~~~~~~~~~~~~~~

.. _setting-up-worldshare-record-manager-label:

Setting up WorldShare Record Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Select "User Preferences" in the sidebar.

   .. Note::
      
      Since these are *User* Preferences, they must be configured for each user
      who will be using the service.
   
   |worldshare_user_preferences|
   
5. Go to "Exporting – Bibliographic Records".
   
   The official documentation from OCLC for all settings under this heading is
   found at
   https://help.oclc.org/Metadata_Services/WorldShare_Record_Manager/Record_Manager_user_preferences/Exporting_Bibliographic_records#Set_preferences_for_TCP.2FIP_export .
   
6. Go to the "General" tab.
   
   |worldshare_user_preferences_export_biblio_format_utf8|
   
   1. Set the "Format" to "MARC 21 with UTF-8 Unicode", since Koha always uses
      UTF-8.
   2. Click "Save".
7. Go to the "TCP/IP" tab.
   
   |worldshare_user_preferences_export_biblio_tcp_ip|
   
   1. Enter the host name or IP address at which the OCLC connexion daemon
      service (configured :ref:`above <setting-up-oclc-service-on-koha-label>`)
      can be reached.
   2. Set the Authentication to "Login ID" and enter the ``connexion_user`` and
      ``connexion_password`` from the configuration file (created
      :ref:`above <setting-up-oclc-service-on-koha-label>`).
   3. Optionally set the connection delay, connection attempts, and other
      settings.
   4. Click "Save".

Using WorldShare Record Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _sending-record-from-worldshare-to-koha-label:

Sending a single record from the WorldShare Record Manager to Koha
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Search for a record.
   
   |worldshare_search|
   
5. Click on the title of a record to open it.
6. Click on the "Record" dropdown menu, then "Send to", then "Local System (via
   TCP/IP)"
   
   |worldshare_record_menu_export|
   
7. After a while, depending on your "connection delay" settings, you should see
   the record in Koha.
   
   - If you used ``import_mode: direct`` in your configuration file, the record
     will be available in the catalog.
   - If you used ``import_mode: staged`` in your configuration file, the record
     will be :ref:`staged for import <stage-marc-records-for-import-label>`.

Sending multiple records from the WorldShare Record Manager to Koha
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Search for a record.
   
   |worldshare_search|
   
5. Click on the title of a record to open it.
6. Click on the "Record" dropdown menu, then "Send to", then "Export List ...".
   Select a list to which to add the record.
7. Repeat from step 4 as needed.
8. Click on "Export Lists" in the sidebar.
9. Click on the name of a list to open it.
10. Click on the "Export" dropdown menu, then "Send to local system (via
    TCP/IP)".
   
   |worldshare_export_list|
   
11. After a while, depending on your "connection delay" settings, you should see
    the record in Koha.
   
   - If you used ``import_mode: direct`` in your configuration file, the record
     will be available in the catalog.
   - If you used ``import_mode: staged`` in your configuration file, the record
     will be :ref:`staged for import <stage-marc-records-for-import-label>`.

OCLC Connexion Client desktop software
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

See https://wiki.koha-community.org/wiki/OCLC_Connexion_Client 
for instructions on setting up and using the OCLC 
Connexion Client desktop software.

.. _talking-tech-label:

Talking Tech
-------------------------------------------------

Talking Tech i-tiva is a third party, proprietary, product that
libraries can subscribe to. Learn more here:
http://www.talkingtech.com/solutions/library.

.. _installation-and-setup-instructions-label:

Installation and setup instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Be sure you've run
installer/data/mysql/atomicupdate/Bug-4246-Talking-Tech-itiva-phone-notifications.pl
to install the required data pack (new syspref, notice placeholders and
messaging transport preferences)

To use,
:ref:`TalkingTechItivaPhoneNotification <talkingtechitivaphonenotification-label>`
system preference must be turned on.

If you wish to process PREOVERDUE or RESERVES messages, you'll need the
:ref:`EnhancedMessagingPreferences <EnhancedMessagingPreferences-label>` system
preference turned on, and patrons to have filled in a preference for
receiving these notices by phone.

For OVERDUE messages, overdue notice triggers must be configured under
More > Tools > :ref:`Overdue notice/status triggers <overdue-notice/status-triggers-label>`. Either
branch-specific triggers or the default level triggers may be used
(script will select whichever is appropriate).

.. _sending-notices-file-label:

Sending notices file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Add the :ref:`TalkingTech\_itiva\_outbound.pl <cron-sending-notices-file-label>`
   script to your crontab

2. Add an FTP/SFTP or other transport method to send the output file to
   the i-tiva server

3. If you wish, archive the sent notices file in another directory after
   sending

Run TalkingTech\_itiva\_outbound.pl --help for more information

.. _receiving-results-file-label:

Receiving results file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Add an FTP/SFTP or other transport method to send the Results file to
   a known directory on your Koha server

2. Add the :ref:`TalkingTech\_itiva\_inbound.pl <cron-receiving-notices-file-label>`
   script to your crontab, aimed at that directory

3. If you wish, archive the results file in another directory after
   processing

Run TalkingTech\_itiva\_inbound.pl --help for more information
