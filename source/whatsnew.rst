.. include:: images.rst

.. _whats-new-label:

What's new
===============================================================================

This section highlights new features and enhancements in the latest Koha
releases.

This section is updated as the manual is updated. Therefore, it may not contain
all the new features and enhancements in the release. Please consult the release
notes for an exhaustive list of all changes in Koha for each version.

The version number indicated in the manual is the newest in which the feature or
enhancement appears. Be aware that bugs may have been backported to previous
Koha versions. If this is the case, you may come across a feature in an
earlier version than indicated throughout these pages.

.. _whats-new-24-11-label:

24.11
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 24.11.00 <https://koha-community.org/koha-24-11-00-released/>`_.

.. _whats-new-24-11-bookable-item-types:

Bookable item types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to make items bookable at the
:ref:`item type level <item-types-label>`.


.. _whats-new-24-11-label:

24.11
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 24.11.00 <https://koha-community.org/koha-24-11-00-released/>`_.

.. _whats-new-24-11-noissuescharge-patron-category-label:

Setting checkout blocking limits at the patron category level
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The system preferences :ref:`noissuescharge <noissuescharge-label>`,
:ref:`NoIssuesChargeGuarantees <noissueschargeguarantees-label>`, and
:ref:`NoIssuesChargeGuarantorsWithGuarantees <noissueschargeguarantorswithguarantees-label>`
are used to determine the maximum outstanding balance a patron can have before
checkouts are blocked. These limits can now be overridden at the
:ref:`patron category <adding-a-patron-category-label>` level, enabling
different thresholds for different patron categories.

.. _whats-new-24-11-new-system-preferences-label:

New system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**ForcePasswordResetWhenSetByStaff**

This new system preference can be used to force patrons whose accounts were
manually created by staff patrons (as opposed to self-registered patrons) to
change their password when they log into the OPAC for the first time.

The setting can also be set at the
:ref:`patron category <adding-a-patron-category-label>` level.

.. _whats-new-24-11-modified-system-preferences-label:

Modified system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**BlockExpiredPatronOpacActions**

It is now possible to determine which actions are blocked in the OPAC when a
patron's account is expired: placing a hold on an item, placing an ill request,
or renewing an item. Previously, the
:ref:`BlockExpiredPatronOpacActions <blockexpiredpatronopacactions-label>`
system preference was a simple on/off switch. It now allows more flexibility.

The system preference can be overridden by the setting in
:ref:`patron categories <adding-a-patron-category-label>`.

.. _whats-new-24-05-label:

24.05
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 24.05.00 <https://koha-community.org/koha-24-05-00-released/>`_.

.. _whats-new-24-05-erm-label:

E-resource management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can now :ref:`import local title records into Koha from a KBART file <import-from-kbart-label>`, making it easier
to get your eholdings records into Koha.

.. _whats-new-24-05-record-sources-label:

Record sources and locked records
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This new feature allows libraries to define
:ref:`record sources <record-sources-label>`, with the option to 'lock' the
records from specific sources to prevent them from being edited in Koha.

This is particularly useful in instances where records are catalogued in another
system and pushed to Koha. Sometimes, in those cases, it's best to edit the
records in the source system rather than in Koha.

In version 24.05, the records' sources can only be set when adding records using
the API. Eventually, it will be possible to manually set the record's source,
but it is not currently the case.

This new feature comes with new permissions as well:

-  :ref:`manage\_record\_sources <permission-manage-record-sources-label>`:
   staff with this permission will be able to create, edit, and delete record
   sources.

-  :ref:`edit\_locked\_records <permission-edit-locked-records-label>`: staff
   with this permission will be able to edit records whose source should
   protect them from modification.

.. _whats-new-24-05-date-patron-attributes-label:

Date patron attribute types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:ref:`Patron attribute types <patron-attribute-types-label>` can now be defined
as dates. When :ref:`adding a new patron attribute type <adding-patron-attributes-label>`,
check the 'Is a date' checkbox. This will add a date picker to the field when
filling out the patron form. You will also be able to do date calculations
based on this attribute in reports.

.. _whats-new-24-05-library-hours-label:

Library opening hours
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Opening hours can be defined in :ref:`Libraries <libraries-label>`. These are
used for hourly loans. The new
:ref:`ConsiderLibraryHoursInCirculation <considerlibraryhoursincirculation-label>`
system preference determines how the due time should be calculated if it falls
after the library's opening hours.

.. _whats-new-24-05-catalog-concerns-label:

New functionalities in catalog concerns feature
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`catalog concerns feature <catalog-concerns-label>` has been developed
further in this cycle.

You can now define custom statuses in the new
:ref:`TICKET\_STATUS authorized value category <ticketstatus-av-category-label>`
and these will appear when updating
:ref:`catalog concerns <manage-catalog-concerns-label>`.

You can now define custom resolutions in the new
:ref:`TICKET\_RESOLUTION authorized value category <ticketresolution-av-category-label>`
and these will appear when marking
:ref:`catalog concerns <manage-catalog-concerns-label>` as 'Resolved'.

It is now possible to assign catalog concerns to staff members when
:ref:`managing catalog concerns <manage-catalog-concerns-label>`. They will
receive an email based on the TICKET\_ASSIGNED template, which is customizable
in the :ref:`notices and slips tool <notices-and-slips-label>`.

.. _whats-new-24-05-copy-permissions-label:

Copying patron permissions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A 'Copy settings' button has been added to the
:ref:`patron permissions <setting-patron-permissions-label>` as well as a
'Paste permissions' button, which can be used to copy permissions from one
patron to another,

.. _whats-new-24-05-patron-email-messages-label:

Sending custom emails to a patron
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new :ref:`custom email message feature <patron-emails-label>` adds the
ability for staff with the necessary permission to send custom emails to a
specific patron. Notices can be defined in advance through the
:ref:`notices and slips tool <notices-and-slips-label>` (see the 'Patrons
(custom message)' module when adding a new notice), or an email can be written
in an ad hoc manner.

.. _whats-new-24-05-format-notices-label:

Formatting printed notices and slips
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In addition to the existing :ref:`SlipCSS <slipcss-label>` and
:ref:`NoticeCSS <noticecss-label>` system preferences, which require a path to
a CSS file, printed notices and slips can now be styled individually with CSS
using the 'Format' tab in the
:ref:`notices and slips tool <notices-and-slips-label>`.

.. _whats-new-24-05-elasticsearch-configuration-label:

Elasticsearch  configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the :ref:`search engine configuration <search-engine-configuration-label>`
it is now possible to add new search fields from the staff interface.

The customization of facets was also extended. It is now possible to
:ref:`add new facets <search-engine-configuration-facets-label>` and link
facets to authorized value categories to improve the display.

.. _whats-new-24-05-serial-numbering-placeholders-label:

New placeholders for serial numbering patterns
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New placeholders can now be used in
:ref:`serial numbering patterns <adding-serial-numbering-pattern-label>` in
addition to the three existing variables {X}, {Y}, and {Z}:

-  {Day} will be replaced by the date (two digits)

-  {Month} will be replaced by the month number (two digits)

-  {Year} will be replaced by the year (four digits)

-  {DayName} will be replaced by the name of the day of the week

-  {MonthName} will be replaced by the name of the month

.. _whats-new-24-05-html-customization-new-locations-label:

New display locations for HTML customizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are new display locations for the
:ref:`HTML customization <html-customizations-label>` tool.

-  ILLModuleCopyrightClearance: content will appear when
   :ref:`creating an interlibrary loan request on the OPAC <your-interlibrary-loan-requests-label>`.
   This display location replaces the deprecated ILLModuleCopyrightClearance
   system preference.

.. _whats-new-24-05-sysprefs-label:

New system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**1PageOrderPDFText**

The new :ref:`1PageOrderPDFText <1pageorderpdftext-label>` system preference
allows libraries to customize the text above the order table in a
:ref:`basket group <create-a-basket-group-label>` PDF. This feature is
available when using the 'English 1-Page' option in the
:ref:`OrderPdfFormat <orderpdfformat-label>` system preference.

**AutoClaimReturnStatusOnCheckin**

The new :ref:`AutoClaimReturnStatusOnCheckin <autoclaimreturnstatusoncheckin-label>`
can be used to automatically
:ref:`resolve return claims <resolve-return-claim-label>` when the item is
checked in.

**AutoClaimReturnStatusOnCheckout**

The new :ref:`AutoClaimReturnStatusOnCheckout <autoclaimreturnstatusoncheckout-label>`
can be used to automatically
:ref:`resolve return claims <resolve-return-claim-label>` when the item is
checked out.

**DefaultLongOverduePatronCategories**

The new :ref:`DefaultLongOverduePatronCategories <defaultlongoverduepatroncategories-label>`
system preference can be used to limit the automatic long overdue process to
specific patron categories. It replaces the :code:`--category` parameter in the
:ref:`longoverdue cron job <cron-long-overdues-label>`.

**DefaultLongOverdueSkipPatronCategories**

The new :ref:`DefaultLongOverdueSkipPatronCategories <defaultlongoverdueskippatroncategories-label>`
system preference can be used to *exclude* specific patron categories from the
automatic long overdue process. It replaces the :code:`--skip-category`
parameter in the :ref:`longoverdue cron job <cron-long-overdues-label>`.

**EmailFieldSelection**

The new :ref:`EmailFieldSelection <emailfieldselection-label>` system
preference, in conjunction with a new 'selected addresses' option in the
:ref:`EmailFieldPrimary <emailfieldprimary-label>` system preference, can be
used to send email notices to multiple email addresses. For example, a library
could choose to send notices to a patron's home and work email addresses.

**ESPreventAutoTruncate**

The new :ref:`ESPreventAutoTruncate <espreventautotruncate-label>` system
preference determines which Elasticsearch indexes are not autotruncated. This
is useful for indexes for identifiers such as barcode and control-number, as
truncating an identifier can return many irrelevant results.

**DisplayMultiItemHolds**

The new :ref:`DisplayMultiItemHolds <displaymultiitemholds-label>` system
preference can be used by libraries to allow to place multiple item-level holds
on a single record at the same time for the same patron. This applies to both
the staff interface and the OPAC.

**HoldRatioDefault**

The new :ref:`HoldRatioDefault <holdratiodefault-label>` system preference
enables libraries to choose the default value for the
:ref:`hold ratios report <hold-ratios-label>`.

**OPACAuthorIdentifiersAndInformation**

The :ref:`OPACAuthorIdentifiersAndInformation <opacauthoridentifiersandinformation-label>`
system preference replaces the :ref:`OPACAuthorIdentifiers <opacauthoridentifiers-label>`
system preference. It is now used to display more information than simply the
identifiers, such as the places of birth and death, the field of activity, etc.

**PurgeListShareInvitesOlderThan**

The :ref:`PurgeListShareInvitesOlderThan <purgelistshareinvitesolderthan-label>`
system preference is used to define a number of days after which private list
shares that have not been accepted are deleted.

**RedirectToSoleResult**

The new :ref:`RedirectToSoleResult <redirecttosoleresult-label>` system
preference allows libraries to determine the behavior when a search returns
only one result, whether or not the user is redirected to the detailed record.
The default is to redirect to the detailed record, which is the historical
behavior.

**RESTAPIRenewalBranch**

The new :ref:`RESTAPIRenewalBranch <restapirenewalbranch-label>` system
preference allows libraries to choose which branchcode is stored in the
statistics table when a renewal is done through the REST API. This helps for
reports. The default is the API user library, which is the historical behavior.

**SCOBatchCheckoutsValidCategories**

The new :ref:`SCOBatchCheckoutsValidCategories <scobatchcheckoutsvalidcategories-label>`
system preference, used with the :ref:`BatchCheckouts <BatchCheckouts-label>`
and :ref:`WebBasedSelfCheck <WebBasedSelfCheck-label>` system preferences,
lets patrons of specific categories checkout several items at once in the
:ref:`self checkout module <self-checkout-label>` rather than one by one.

**SMSSendAdditionalOptions**

The new :ref:`SMSSendAdditionalOptions <smssenduserdetails-label>`
system preference can be used to specify additional parameters needed by some
SMS::Send drivers (such as Twilio). Previously, it was necessary to have any
additional parameters in a YAML file named after the SMS::Send driver, and the
path to that file needed to be specified in the koha-conf.xml file. With the
system preference, the options can be updated without the need for a system
administrator to change the file.

**StaffLoginLibraryBasedOnIP**

The new :ref:`StaffLoginLibraryBasedOnIP <staffloginlibrarybasedonip-label>`
system preference can be used to determine which library staff log into when
accessing the staff interface, based on their computer's IP address.

**StaffLoginRestrictLibraryByIP**

The :ref:`StaffLoginRestrictLibraryByIP <staffloginrestrictlibrarybyip-label>`
system preference replaces the AutoLocation system preference. It is used to
determine which libraries a staff member can choose to log into when accessing
the staff interface.

**WaitingNotifyAtCheckout**

The new :ref:`WaitingNotifyAtCheckout <waitingnotifyatcheckout-label>`
generates a pop-up in the circulation module alerting staff that the patron
they are checking out items to also has holds waiting for them.

.. _whats-new-24-05-deprecated-sysprefs-label:

Deprecated system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**ILLModuleCopyrightClearance**

The ILLModuleCopyrightClearance system preference was moved to an
:ref:`HTML customization location <html-customizations-label>`. This allows to
have texts in different languages in a multilingual OPAC.

.. _whats-new-23-11-label:

23.11
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 23.11.00 <https://koha-community.org/koha-23-11-released/>`_.

.. _whats-new-23-11-preservation:

Preservation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The brand new :ref:`Preservation module <preservation-label>` is used for integrating
preservation treatments into the Koha workflow and keep track of them. For every single
step of the preservation workflow, data is attached to the Koha items.

The module comes with its own set of :ref:`system preferences<preservation-system-preferences-label>`.

.. _whats-new-23-11-bookings-label:

Item bookings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :ref:`booking <bookings-label>` is a new way to reserve an item. Where a
:ref:`hold <holds-circulation-label>` is a reservation that joins a waiting list
and is filled as soon as the item is available, a booking is an advance
reservation filled for the dates specified.

Bookings are useful for items such as models, story sacks, memory boxes, etc.
that are used for events and programs at specific dates.

Bookings can only be :ref:`placed in the staff interface <place-booking-label>`,
on items that have been made :ref:`bookable <make-items-bookable-label>`.

.. _whats-new-23-11-protected-patrons-label:

Protected patrons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to protect patrons from deletion. When
:ref:`adding <add-a-new-patron-label>` or
:ref:`editing a patron <editing-patrons-label>`, a new 'Protected' flag can be
set in the 'Library management' section. This will disable the 'Delete' option
in the patron file. Furthermore, protected patrons cannot be deleted by batch
deletion, cron jobs, or patron merging.

Use this for your statistical patrons, SIP2 users, self checkout users and
superadmins.

.. _whats-new-23-11-custom-slips-label:

Custom slips
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to create custom slips that will be available from the
:ref:`'Print' menu in a patron's account <printing-receipts-label>`.

Go to :ref:`Tools > Notices and slips <notices-and-slips-label>` and
:ref:`create a new slip <adding-notices-and-slips-label>` with the new 'Patrons
(custom slip)' category.

.. _whats-new-23-11-custom-report-templates-label:

Custom report templates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to create templates in the
:ref:`notices and slips tool <notices-and-slips-label>` and use those in the
:ref:`reports module <reports-label>` when
:ref:`running SQL reports <running-custom-reports-label>`.

It is also possible to download the report results as the rendered template.

.. _whats-new-23-11-vendor-issues-label:

Vendor issues
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to :ref:`record problems with vendors <vendor-issues-label>`
in the acquisitions module. It is a way to keep track of the various issues that
might arise in the course of a contract, and it might be helpful when the time
comes to renegotiate.

A new patron permission :ref:`issue\_manage <permission-issue-manage-label>`
was also added to allow libraries to choose who among the staff can manage
vendor issues.

.. _whats-new-23-11-brach-level-userjs-usercss-label:

Library-level OPAC CSS and JS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to add custom CSS and JavaScript to an OPAC for a specific
library. Two new fields were added to the
:ref:`library form <adding-a-library-label>`:

-  UserJS

-  UserCSS

This code will be used when a patron logs in to the OPAC, or if the system has
more than one OPAC (through apache configuration files).

.. _whats-new-23-11-offline-circulation-label:

Deprecation of the offline circulation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`offline circulation module <offline-circulation-in-koha-label>` has
been deprecated. Only the :ref:`Firefox plugin <firefox-plugin-label>` and
:ref:`Windows tool <offline-circ-tool-for-windows-label>` are now supported.

Accordingly, the :ref:`AllowOfflineCirculation <allowofflinecirculation-label>`
system preference was removed.

.. _whats-new-23-11-serial-publication-text-template-label:

Template for serial publication date
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to create a template to automatically fill the 'Published on
(text)' field when receiving a new serial. This is set at the
:ref:`subscription-level <add-a-subscription-label>`, therefore, you can have
different templates for different subscriptions.

.. _whats-new-23-11-cookies-label:

Cookies information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New system preferences and HTML customizations allow for cookies information to be
:ref:`added to the OPAC <opac-cookies-label>` and staff interface.

-  Add your cookie banner text to the CookieConsentBar :ref:`HTML customization <html-customizations-label>`.

-  Add your cookies policy to the CookieConsentPopup :ref:`HTML customization <html-customizations-label>`.

-  Configure your non-essential cookies in the :ref:`CookieConsentedJS system preference <cookieconsentedjs-syspref>`.

-  Switch on the :ref:`CookieConsent system preference <cookieconsent-syspref>`.

.. _whats-new-23-11-opac-self-checkout-label:

OPAC self checkout
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new :ref:`OPAC self checkout <opac-self-checkout-label>` feature allows
patrons to check out items to themselves via the OPAC.

This is different from the :ref:`self checkout module <self-checkout-label>`,
which is a module onto itself, meant to be used on dedicated computers. The
OPAC self checkout can be used on any computer or device that has access to the
OPAC.

It is enabled with the :ref:`OpacTrustedCheckout <opactrustedcheckout-label>`
system preference.

.. _whats-new-23-11-barcode-width-height-label:

Barcode width and height customizable on labels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In :ref:`label layouts <label-layouts-label>`, it is now possible to customize
the width and height of the barcodes.

.. _whats-new-23-11-local-float-groups-label:

Local float groups
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A new option in :ref:`library groups <library-groups-label>` can be used to
create 'local float groups' which can then be set in
:ref:`return policies <default-checkouts-and-hold-policy-label>` or in
:ref:`hold policies <item-hold-policies-label>` in circulation rules to
determine if an item 'floats' (stays at the check-in library) or is transferred
back to its home library when checked in.

.. _whats-new-23-11-hold-pickup-delay-label:

New circulation rule for holds pickup delays
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A new column in the
:ref:`circulation and fine rules <defining-circulation-rules-label>` table has
been added to control the hold pickup delays on a more granular level than with
the :ref:`ReservesMaxPickUpDelay <reservesmaxpickupdelay-label>` system
preference. By having it in the circulation rules matrix, it is possible to set
different rules for different combinations of library, patron category, and
item type.

.. _whats-new-23-11-no-automatic-renewal-before-label:

New 'No automatic renewal before' circulation rule
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The 'No renewal before' :ref:`circulation rule <defining-circulation-rules-label>`
has been separated into 'No renewal before' and 'No automatic renewal before'.
This way, libraries that use the automatic renewal can control this value
independently of manual renewals.

.. _what-new-23-11-ill-label:

ILL features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A new option 'Can place ILL in OPAC' in the
:ref:`patron categories <patron-categories-label>` allows libraries to choose
which patron categories are allowed to place ILL requests from the OPAC.

.. _whats-new-23-11-patron-attributes-label:

Search patron attributes in 'standard' patron search
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may now define some of your
:ref:`patron attribute types <patron-attribute-types-label>` as searchable by
default, meaning that a standard patron search, without any specified search
field, will also search in the values of this attribute.

.. _whats-new-23-11-patron-lists-tab-label:

Patron lists tab in patron records
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is a new :ref:`patron lists tab <patron-lists-tab-label>` in patron
records, showing which :ref:`patron lists <patron-lists-label>` this patron is
a part of.

.. _whats-new-23-11-batch-operations-from-item-search-label:

Batch operations from item search
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When using the :ref:`item search <item-searching-label>`, you can now select
items and send them directly to the
:ref:`batch item modification tool <batch-item-modification-label>` or to the
:ref:`batch item deletion tool <batch-item-deletion-label>`.

.. _whats-new-23-11-elasticsearch-configuration-label:

Elasticsearch configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the :ref:`search engine configuration <search-engine-configuration-label>`
it is now possible to add a filter on specific mappings to index those fields
without punctuation. This will enable searching without punctuation. For
example, if there is a punctuation filter on the title, and the title is
"1,000 Japanese Words", a search for "1000 japanese words" will return the
title.

.. _whats-new-23-11-cost-quantity-manual-invoice-label:

Specify cost and quantity in manual invoices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When :ref:`creating a manual invoice <creating-manual-invoices-label>` for a
:ref:`custom debit type <debit-types-label>`, there will be a 'Cost' and a
'Quantity' field where you will be able to enter the cost of the individual
item and the quantity to be charged to the patron. The total amount will
automatically be calculated from these two values. This is useful mainly for
physical items being sold to the patron, such as used books or reusable bags,
for example.

.. _whats-new-23-11-serial-alerts-label:

List of patron's serial alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Patrons can subscribe to email alerts for new serial issues from the OPAC.
They can now
:ref:`view a list of the serials they've chosen to receive alerts for <opac-alert-subscriptions-label>`
in their online account. This page also allows them to unsubscribe from alerts
if needed.

Staff also has access to the
:ref:`list of email alerts in the patron's account <patron-alert-subscriptions-label>`
and can unsubscribe patrons.

.. _whats-new-23-11-html-customization-new-locations-label:

New display locations for HTML customizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are several new display locations for the
:ref:`HTML customization <html-customizations-label>` tool.

-  StaffAcquisitionsHome: content will appear at the bottom of the
   :ref:`Acquisitions module <acquisitions-label>` main page.

-  StaffAuthoritiesHome: content will appear at the bottom of the
   :ref:`Authorities <authorities-label>` main page.

-  StaffCataloguingHome: content will appear at the bottom of the
   :ref:`Cataloguing module <cataloging-label>` main page.

-  StaffListsHome: content will appear at the bottom of the
   :ref:`Lists <lists-label>` main page.

-  StaffPatronsHome: content will appear at the bottom of the
   :ref:`Patrons module <patrons-label>` main page.

-  StaffPOSHome: content will appear at the bottom of the
   :ref:`Point of sale <point-of-sale-label>` main page.

-  StaffSerialsHome: content will appear at the bottom of the
   :ref:`Serials module <serials-label>` main page.

The move of system preferences to HTML customizations continues. These were
moved to the :ref:`HTML customization tool <html-customizations-label>` in
version 23.11.

-  OpacMaintenanceNotice

-  OPACResultsSidebar

-  OpacSuppressionMessage

-  PatronSelfRegistrationAdditionalInstructions

-  SCOMainUserBlock

-  SelfCheckHelpMessage

-  SelfCheckInMainUserBlock

.. _whats-new-23-11-sysprefs-label:

New system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**AcquisitionsDefaultEmailAddress**

The new :ref:`AcquisitionsDefaultEmailAddress <acquisitionsdefaultemailaddress-label>`
system preference allows you to set a specific email address that will be used to send orders and
late order claims from the acquisitions module.

**AcquisitionsDefaultReplyTo**

The new :ref:`AcquisitionsDefaultReplyTo <acquisitionsdefaultreplyto-label>`
system preference allows you to set a specific reply-to email address that will receive replies to
orders and late order claims sent from the acquisitions module.

**AutomaticCheckinAutoFill**

The new :ref:`AutomaticCheckinAutoFill <automaticcheckinautofill-label>`
system preference further automates the
:ref:`automatic check-in feature <cron-automatic-checkin-label>` so that the next
hold is automatically filled when an item is checked in.

**AutoRemoveOverduesRestrictions**

While not a new system preference, the options for
:ref:`AutoRemoveOverduesRestrictions <autoremoveoverduesrestrictions-label>`
were expanded to check for other checkouts in the patron's account before
lifting the restriction.

**CalculateFundValuesIncludingTax**

The new :ref:`CalculateFundValuesIncludingTax <calculatefundvaluesincludingtax-label>`
system preference will help libraries that claim back tax on purchases.
By setting the preference to 'Exclude', these libraries can directly input in
Koha order prices with tax included (as printed by the vendor on their invoice);
and the library funds will show the amounts excluding tax, which is an accurate
reflection of the amounts actually available.

**CancelOrdersInClosedBaskets**

The new :ref:`CancelOrdersInClosedBaskets <cancelordersinclosedbaskets-label>`
system preference can be set to allow
:ref:`cancelling acquisitions orders <cancelling-an-order-label>` in baskets
that are closed. This is useful if something cannot be delivered and you don't
want to reopen the basket or go through the receive shipment process.

**ChildNeedsGuarantor**

The new :ref:`ChildNeedsGuarantor <childneedsguarantor-label>` system preference
can be used to make the guarantor field mandatory. Contrary to its name and
description, it is not limited to children, but to patrons whose
:ref:`patron category <patron-categories-label>` states that they 'can be
guarantee'.

**DefaultAuthorityTab**

The new :ref:`DefaultAuthorityTab <defaultauthoritytab-label>` system preference
allows libraries to choose which tab is selected first when viewing an authority
record.

**EmailPatronWhenHoldIsPlaced**

The new :ref:`EmailPatronWhenHoldIsPlaced <emailpatronwhenholdisplaced-label>`
system preference can be used to send emails to patrons who place holds. The
notice sent is based on the HOLDPLACED\_PATRON template, which can be
customized in the :ref:`notices and slips tool <notices-and-slips-label>`.

**ForceLibrarySelection**

The new :ref:`ForceLibrarySelection <forcelibraryselection-label>` system preference can be used
to require staff to choose a library when logging into the staff interface.

**ILLModuleDisclaimerByType**

The new :ref:`ILLModuleDisclaimerByType <illmoduledisclaimerbytype-label>`
system preference allows libraries to have different disclaimer texts for
different interlibrary loan request types. This depends on the backend used for
ILL requests.

**ILLPartnerCode**

The new :ref:`ILLPartnerCode <illpartnercode-label>` system preference replaces
an :ref:`interlibrary loan <ill-requests-label>` setting that was only found in
the koha-conf.xml file. It is now possible for libraries to choose the partner
category without having to change the configuration file.

**IntranetReadingHistoryHolds**

The new :ref:`IntranetReadingHistoryHolds <intranetreadinghistoryholds-label>`
system preference is an offshoot of the existing
:ref:`intranetreadinghistory <intranetreadinghistory-label>` system preference.
It used to be that the :ref:`intranetreadinghistory <intranetreadinghistory-label>`
system preference controlled the display of both the
:ref:`Circulation history <circulation-history-label>` and the
:ref:`Holds history <holds-history-label>` tabs in a patron's account in
the staff interface. Now the
:ref:`intranetreadinghistory <intranetreadinghistory-label>` system preference
controls the display of the :ref:`Circulation history tab <circulation-history-label>`
and the new :ref:`IntranetReadingHistoryHolds <intranetreadinghistoryholds-label>`
system preference controls the display of the
:ref:`Holds history tab <holds-history-label>`.

**LoadCheckoutsTableDelay**

The new :ref:`LoadCheckoutsTableDelay <loadcheckoutstabledelay-label>` system
preference can be used to delay the loading of the checkouts table in a
patron's account to prevent too many service queries when checking out a number
of items in a row.

**OpacTrustedCheckout**

The new :ref:`OpacTrustedCheckout <opactrustedcheckout-label>` system preference
enables libraries with trusted communities to allow patrons to
:ref:`check out to themselves via the OPAC <opac-self-checkout-label>`. This is
different from the :ref:`self checkout module <self-checkout-label>`, which is
a module onto itself, meant to be used on dedicated computers. The OPAC self
checkout can be used on any computer or device that has access to the OPAC.

**OverdueNoticeFrom**

The :ref:`OverdueNoticeFrom <overduenoticefrom-label>` system preference
already existed, but a new option was added to it in version 23.11. The new
option 'patron home library', allows libraries to choose that library as the
source of information for overdue notices.

**RedirectGuaranteeEmail**

The new :ref:`RedirectGuaranteeEmail <redirectguaranteeemail-label>` system
preference allows libraries to send copies of email notices to the patron and
the patron's guarantor, if any.

**SCOLoadCheckoutsByDefault**

The new :ref:`SCOLoadCheckoutsByDefault <scoloadcheckoutsbydefault-label>`
system preference allows libraries to choose if the patron's current checkouts
are displayed automatically or if the patron must click a button to see their
checkouts when using the self checkout module.

**SerialsDefaultEmailAddress**

The new :ref:`SerialsDefaultEmailAddress <serialsdefaultemailaddress-label>`
system preference allows you to set a specific email address that will be used
to send late serial issues claims from the serials module.

**SerialsDefaultReplyTo**

The new :ref:`SerialsDefaultReplyTo <serialsdefaultreplyto-label>`
system preference allows you to set a specific reply-to email address that will receive replies to
late serial issues claims sent from the serials module.

**SerialsSearchResultsLimit**

The new :ref:`SerialsSearchResultsLimit <serialssearchresultslimit-label>` system
preference allows you to limit the number of serial subscription search results
per page, to be used in systems where there are a lot of subscriptions.

**showLastPatronCount**

The new :ref:`showLastPatronCount <showlastpatroncount-label>` system preference
allows you to choose how many patrons are shown by the link created by
:ref:`showLastPatron <showLastPatron-label>`.

**SIP2AddOpacMessagesToScreenMessage**

The new :ref:`SIP2AddOpacMessagesToScreenMessage <sip2addopacmessagestoscreenmessage-label>`
system preference determines whether or not the
:ref:`OPAC messages <opac-messages-label>` are displayed on the screen when
using SIP2.

**TrackLastPatronActivityTriggers**

The new :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
system preference replaces the
:ref:`TrackLastPatronActivity <tracklastpatronactivity-label>` system preference
and allows a more granular control of which action triggers the update of the
patron's "last seen" date (borrowers.lastseen). Previously, this database column
was only updated when the patron logged into the OPAC or via SIP2. But this
excluded patrons who might check out a lot of items from the library, but never
log into the OPAC. The library can now decide which activities to track from a
list.

**UpdateItemLocationOnCheckout**

The new :ref:`UpdateItemLocationOnCheckout <updateitemlocationoncheckout-label>`
system preference allows you to automatically change an item's location when it
is checked out.

**UpdateItemLostStatusWhenPaid**

The new :ref:`UpdateItemLostStatusWhenPaid <updateitemloststatuswhenpaid-label>`
system preference allows you to set a status which will be automatically
assigned to a lost item when the patron pays for it.

**UpdateItemLostStatusWhenWriteoff**

The new :ref:`UpdateItemLostStatusWhenWriteoff <updateitemloststatuswhenwriteoff-label>`
system preference allows you to set a status which will be automatically
assigned to a lost item when the charge is written off from the patron's account.

**UpdateNotForLoanStatusOnCheckin**

While not a new system preference,
:ref:`UpdateNotForLoanStatusOnCheckin <updatenotforloanstatusoncheckin-label>`
was expanded to add the ability to define item type-specific rules.

.. _whats-new-23-11-patron-permissions-label:

New patron permissions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`preservation permission <permission-preservation-label>` has been
added to allow staff members to access the new
:ref:`preservation module <preservation-label>`.

The :ref:`issue\_manage permission <permission-issue-manage-label>`
has also been added to allow libraries to choose who among the staff can manage
vendor issues.

.. _whats-new-23-11-messaging-preferences-label:

Patron messaging preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For libraries that use
:ref:`EnhancedMessagingPreferences <enhancedmessagingpreferences-label>`, the
'Hold filled' message is now available as a digest. The notice template used is
HOLDDGST and can be customized in the
:ref:`notices and slips tool <notices-and-slips-label>`. Staff can choose the
'Digests only' option in patron files. If the
:ref:`EnhancedMessagingPreferencesOPAC <EnhancedMessagingPreferencesOPAC-label>`
system preference is set to 'show', patrons will be able to modify their
messaging preferences in their online account.

.. _whats-new-23-11-command-line-tools-label:

New options for command line tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`runreport.pl <cron-runreport-label>` script has two new parameters:

-  :code:`--send_empty` adds the option to send the email even if the report
   returns no results

-  :code:`--quote` adds the option to specify the quote character for CSV output

The :ref:`writeoff_debts.pl <cron-writeoff-debt-label>` script has two new parameters:

-  :code:`--category-code` adds the option to limit writeoffs to a specific
   patron category

-  :code:`--added-after` adds the option to limit writeoffs to charges added
   after a specific or calculated date

The :ref:`borrowers-force-messaging-defaults.pl <cron-borrowers-messaging-preferences-label>`
script has two new parameters:

-  :code:`--library` adds the option to limit updates to patrons from a
   specific library

-  :code:`--message-name` adds the option to limit updates to a specific message

The :ref:`membership\_expiry.pl <cron-notify-patrons-of-expiration-label>`
script has four new parameters:

-  :code:`-active` adds the option to send notices to "active" patrons only.
   Activity is determined by the new
   :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
   system preference.

-  :code:`-inactive` adds the option to send notices to "inactive" patrons only.
   Activity is determined by the new
   :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
   system preference.

-  :code:`-renew` adds the option to automatically renew patron memberships
   instead of simply advising them that their membership is about to expire.

-  :code:`-letter_renew` adds the option to use a different notice than the
   default one, MEMBERSHIP\_RENEWED.

A new command line tool,
:ref:`debar_patrons_with_fines.pl <cron-debar-patrons-with-fines-label>`, was
added. This tool adds manual restrictions to patron accounts who owe more than
a specified amount in charges.